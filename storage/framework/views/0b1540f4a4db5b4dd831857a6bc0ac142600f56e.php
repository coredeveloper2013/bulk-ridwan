



<?php $__env->startSection('content'); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						<div class="list-group">
						  	<a class="list-group-item <?php if(\Request::route()->getName()=='admin'): ?> active <?php endif; ?>" href="/admin/">Overview</a>
						  	<a class="list-group-item <?php if(\Request::route()->getName()=='admin/manage-user'): ?> active <?php endif; ?>" href="/admin/manage-user/">Manage User</a>
						  	<a class="list-group-item <?php if(\Request::route()->getName()=='admin/membership-plan'): ?> active <?php endif; ?>" href="/admin/membership-plan">Membership Plan</a>
						</div>
					</div>
					<div class="col-md-9">
						<ul class="list-inline">
							<li> <a class="btn btn-primary" href="/admin/membership-plan/add">Add plan</a> </li>
						</ul>
						<hr>

<h4>Monthly Plan</h4>
<ul class="list-unstyled">
						<?php $__currentLoopData = $plans_m; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><br><strong><?php echo e($plan->name); ?></strong></li>
						<li>$<?php echo e($plan->price); ?> per month</li>
						<li>Connected Buffer Account:  <?php echo e($plan->connucted_buf_account); ?></li>
						<li>Save Content Upload Post:  <?php echo e($plan->save_content_upload_post); ?> </li>
						<li>Save Content Upload Group:  <?php echo e($plan->save_content_upload_group); ?></li>
						<li>Save Content Curation Feeds:  <?php echo e($plan->save_content_curation_feeds); ?> </li>
						<li>Save Content Curation Group:  <?php echo e($plan->save_content_curation_group); ?> </li>
						<li>Save RSS Automotion Feeds:  <?php echo e($plan->save_rss_auto_feeds); ?></li>
						<li>Save RSS Automotion Group: <?php echo e($plan->save_rss_auto_group); ?></li>
						<li>
						<a class="btn btn-default" href="/admin/membership-plan/edit/<?php echo e($plan->id); ?>">Edit</a> 
						<a class="btn btn-default" href="/admin/membership-plan/delete/<?php echo e($plan->id); ?>">Delete</a>
						</li>
						
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>
<br>
<br>
<h4>Yearly Plan</h4>

<ul class="list-unstyled">
						<?php $__currentLoopData = $plans_y; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $plan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li><br><strong><?php echo e($plan->name); ?></strong></li>
						<li>$<?php echo e($plan->price); ?> per year</li>
						<li>Connected Buffer Account:  <?php echo e($plan->connucted_buf_account); ?></li>
						<li>Save Content Upload Post:  <?php echo e($plan->save_content_upload_post); ?> </li>
						<li>Save Content Upload Group:  <?php echo e($plan->save_content_upload_group); ?></li>
						<li>Save Content Curation Feeds:  <?php echo e($plan->save_content_curation_feeds); ?> </li>
						<li>Save Content Curation Group:  <?php echo e($plan->save_content_curation_group); ?> </li>
						<li>Save RSS Automotion Feeds:  <?php echo e($plan->save_rss_auto_feeds); ?></li>
						<li>Save RSS Automotion Group: <?php echo e($plan->save_rss_auto_group); ?></li>
						<li>
						<a class="btn btn-default" href="/admin/membership-plan/edit/<?php echo e($plan->id); ?>">Edit</a> 
						<a class="btn btn-default" href="/admin/membership-plan/delete/<?php echo e($plan->id); ?>">Delete</a>
						</li>
						


						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</ul>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>