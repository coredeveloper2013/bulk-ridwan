<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
	<title><?php echo e(config('app.name', 'Laravel')); ?></title>
	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="//fast.appcues.com/widget.css">
	<link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
	<!-- Scripts -->
	<script>
		window.Laravel = <?php echo json_encode([
			'csrfToken' => csrf_token(),
			'APP_URL' => env('APP_URL'),
		]); ?>;
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TLNT7S');</script>
	<script type="text/javascript">(function(o){var b="https://api.autopilothq.com/anywhere/",t="23556ef2086d413ca59b399cb5747679decfd69f94864c3b8fbe825622ff831f",a=window.AutopilotAnywhere={_runQueue:[],run:function(){this._runQueue.push(arguments);}},c=encodeURIComponent,s="SCRIPT",d=document,l=d.getElementsByTagName(s)[0],p="t="+c(d.title||"")+"&u="+c(d.location.href||"")+"&r="+c(d.referrer||""),j="text/javascript",z,y;if(!window.Autopilot) window.Autopilot=a;if(o.app) p="devmode=true&"+p;z=function(src,asy){var e=d.createElement(s);e.src=src;e.type=j;e.async=asy;l.parentNode.insertBefore(e,l);};if(!o.noaa){z(b+"aa/"+t+'?'+p,false)};y=function(){z(b+t+'?'+p,true);};if(window.attachEvent){window.attachEvent("onload",y);}else{window.addEventListener("load",y,false);}})({"app":true});</script>
	<style type="text/css">
		.list-group.post_list{
			opacity: 0;
			transition: all .3s;
		}
		.sample_file_link{
			position: absolute;width: 100%;margin-left: -50%;padding: 4px;
		}
		.post-update input[name='text']{
			font-weight: bold;
			font-size: 16px;
		}
		.group-posts .panel-body{
			position: relative;
		}
		.ree_top{
			position: absolute;
			height: 88px;
			background: #f5f8fa;
			left: -1px;
			right: -1px;
			margin-top: 0;
			top: 0px;
		}


		.navbar.left-navbar {
		    z-index: 500;
		}	



		@media(max-width: 768px){
		    
		    
             .navbar.navbar-default.navbar-fixed-top {
                    z-index: 280;
            }		    
		    
			.type-rss-automation .media,
		.type-curation .media,
		.type-upload .media {
		    position: relative;
		}

		.type-rss-automation .media-body,
		.type-curation .media-body,
		.type-upload .media-body {
		    width: 100%;
		    padding-right: 145px !important;
		    overflow: hidden !important;
		    word-wrap: break-word;
		    display: block;
		}

		.type-rss-automation .list-group .media-right,
		.type-curation .list-group .media-right,
		.type-upload .list-group .media-right {
		    position: absolute;
		    right: 0;
		    z-index: 3;
		    top: 0;
		    bottom: 0;
		} 



		.type-rss-automation .list-group .media-right.upit,
		.type-curation .list-group .media-right.upit,
		.type-upload .list-group .media-right.upit{
			z-index: 4
		}	    
		    
		    

.media-right.media-middle.data-sent,
		.type-rss-automation .list-group .media-right.fac,
		.type-rss-automation .list-group .media-right.goo,
		.type-rss-automation .list-group .media-right.lin,
		.type-rss-automation .list-group .media-right.twit{
			display: none;
		}






			.group-col .group-items{
				height: initial !important;
			}
			.channel-activity .media-body, 
			.channel-activity .media-left, 
			.channel-activity.media-right {
			    display: block;
			    width: 100%;
			}
			.channel-activity .media-left #myChart {
				margin: 0 auto;
			}
			.app-body {
			    padding-left: 40px;
			}
            .navbar.navbar-default.navbar-fixed-top {
                left: 0px;
            }
			 .navbar.left-navbar {
			    left: -320px;
			}
			.nav-toggle .navbar.left-navbar {
			    left: 0px;
			}
            .nav-toggle .navbar.navbar-default.navbar-fixed-top {
                    z-index: 280;
            }
            .navbar-toggle {
                margin: 33px 15px;
            }
            .list-inline {
                position: relative;
            }
            .list-inline .dropdown  {
                position: static;
            }
            .dropdown .dropdown-menu.dropdown-pop {
                left: 0px;
                min-width: 460px;
            }
            
.sample_file_link {
	position: initial;
	margin-left: initial;
}
            
.navbar.navbar-default.navbar-fixed-top .navbar-brand {
    float: right;
}
      .navbar.navbar-default.navbar-fixed-top .navbar-header {
          float: right;
      }
  .navbar.navbar-default.navbar-fixed-top .navbar-collapse {
          float: left;
      }
      .navbar.navbar-default.navbar-fixed-top .nav>li{
              float: right;
      } 
       
.navbar.navbar-default.navbar-fixed-top .navbar-nav {
    margin: 0px -15px;
}
       
.navbar-nav .open .dropdown-menu {
    position: absolute; 
    background-color: rgb(255, 255, 255);
    margin-top: 1px;
}
       
.appcues-widget-dropdown.appcues-widget-fixed {
    left: 0 !important;
    margin-left: 20px;
}
         
         
.panel.panel-default.group-single .dropdown{
    position: initial !important;
}
.panel.panel-default.group-single .dropdown .dropdown-menu.dropdown-pop{
    top: initial !important;
    
}
         
  
         
         
            
		}

	</style>
</head>
<body>
	<script src="//fast.appcues.com/25776.js"></script>
	<script>
		Appcues.identify("<?php echo \Auth::user()->id; ?>", { 
			name: "<?php echo \Auth::user()->name; ?>",
			email: "<?php echo \Auth::user()->email; ?>", 
			created_at: "<?php echo \Auth::user()->created_at; ?>", 
		});
	</script>
	<script type="text/javascript">
		Autopilot.run("associate", {
			Email: "<?php echo \Auth::user()->email; ?>",
			FirstName: "<?php echo \Auth::user()->first_name; ?>",
			LastName: "<?php echo \Auth::user()->last_name; ?>",
			custom: {
			 "string--Logged--In": "true"
			}
		});
	</script>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLNT7S" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div id="app">
		<div class="ajax-working">
			<div class="sk-fading-circle">
				<div class="sk-circle1 sk-circle"></div>
				<div class="sk-circle2 sk-circle"></div>
				<div class="sk-circle3 sk-circle"></div>
				<div class="sk-circle4 sk-circle"></div>
				<div class="sk-circle5 sk-circle"></div>
				<div class="sk-circle6 sk-circle"></div>
				<div class="sk-circle7 sk-circle"></div>
				<div class="sk-circle8 sk-circle"></div>
				<div class="sk-circle9 sk-circle"></div>
				<div class="sk-circle10 sk-circle"></div>
				<div class="sk-circle11 sk-circle"></div>
				<div class="sk-circle12 sk-circle"></div>
			</div>
		</div>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<!-- Collapsed Hamburger
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
						<span class="sr-only">Toggle Navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button> -->
					<!-- Branding Image -->
					<a class="navbar-brand" href="#">
						<i class="fa fa-caret-left"></i> <i class="fa fa-bars"></i>
					</a>
				</div>
				<div class="navbar-collapse">
					<!-- Left Side Of Navbar -->

					<!-- Right Side Of Navbar -->
					<ul class="nav navbar-nav navbar-right">
						<!-- Authentication Links -->
						<?php if(Auth::guest()): ?>
							<li><a href="<?php echo e(route('login')); ?>">Login</a></li>
							<li><a href="<?php echo e(route('register')); ?>">Register</a></li>
						<?php else: ?>
							<li>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="fa fa-envelope"></span></a>
							</li>
							<li>
								<a id="my-widget" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<?php if(count(\Bulkly\User::find(Auth::id())->socialaccounts) > 0): ?>
									<img class="navavatar" src="<?php echo e(Bulkly\User::find(Auth::id())->socialaccounts[0]->avatar); ?>"> 
								<?php else: ?>
									<img class="navavatar" src="/images/noavater.png"> 
								<?php endif; ?>
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu navd" role="menu">
									<li>
										<a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
											Logout
										</a>
										<form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
											<?php echo e(csrf_field()); ?>

										</form>
									</li>
								</ul>
							</li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</nav>
		<style type="text/css">
			.dropdown-menu.navd:before{
				content: none !important;
			}
		</style>
		<nav class="navbar left-navbar">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
					<img src="/images/logo.png">
				  </a>
				</div>
				<?php $route = \Request::route()->getName(); ?>
				<ul class="nav navbar-nav">
				<?php if( \Auth::user()->email == env('SU')): ?>
					<li><a href="<?php echo e(url('/admin')); ?>"> <i class="fa fa-user"></i>Admin</a></li>
				<?php endif; ?>
					<li <?php if($route=='home'): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('/')); ?>"> <i class="fa fa-home"></i> Home</a></li>
				<?php if(session()->has('buffer_token')): ?>
					<li <?php if($route=='content-upload' || $route=='content-pending' || $route=='content-active' || $route=='content-completed'): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('/content-upload')); ?>" class="toggle"> <i class="fa fa-upload"></i> Content Upload</a></li>
					<li <?php if($route=='content-curation' || $route=='content-curation-pending' || $route=='content-curation-active' || $route=='content-curation-completed'): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('/content-curation')); ?>" class="toggle"> <i class="fa fa-file-text-o"></i> Content Curation</a></li>
					<li <?php if($route=='rss-automation' || $route=='rss-automation-pending' || $route=='rss-automation-active' || $route=='rss-automation-completed'): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('/rss-automation')); ?>" class="toggle"> <i class="fa fa-rss"></i> RSS Automation</a></li>
					<li <?php if($route=='analytics'): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('/analytics')); ?>"> <i class="fa fa-line-chart"></i> Analytics</a></li>
					<li <?php if($route=='calendar'): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('/calendar')); ?>"> <i class="fa fa-calendar"></i> Calendar</a></li>
					<li <?php if($route=='social-accounts'): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('/social-accounts')); ?>"> <i class="fa fa-user"></i>Social Accounts</a></li>
					<li <?php if($route=='settings'): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('/settings')); ?>"> <i class="fa fa-gear"></i> Settings</a></li>
				<?php endif; ?>
			  	</ul>
			</div><!-- /.container-fluid -->
		</nav>
		<?php echo $__env->yieldContent('content'); ?>
	</div>
	<script src="<?php echo e(asset('js/app.js')); ?>"></script>
	<script src="//fast.appcues.com/widget-bundle.js" type="text/javascript"></script>
	<script type="text/javascript">
	var widget = AppcuesWidget(Appcues.user());
	widget.init("#my-widget", {
		// Optionally specify the position of the content relative to the widget icon.
		// Possible values: "center" (default; alias "bottom", "bottom-center"), "left" (alias "bottom-left"),
		//   "right" (alias "bottom-right"), "top" (alias "top-center"), "top-left", "top-right"
		position: "left",
	});
	$(window).on('load', function(){
		var window_height = $(window).height();
		$('.group-col .group-items').each(function () {
			var top = $(this).offset().top;
			$(this).height(window_height - top);
		})	
		$('.app-body').each(function () {
			var top = $(this).offset().top + 140;
			$(this).height(window_height - top);
		})
		$('.dropdown-pop.post-to').each(function(){
			var top = $(this).parent().offset().top + 336;
			var need = window_height - top;
			console.log(need);
			if(need<0){
			$(this).css({
				'top' : need +'px'
			})				
			}
		});
		$(".post_list").sortable({
			start: function (event, ui) {
				$('input[name=sorting]').val('1').change();
			},
			stop: function (event, ui) {
				var moved = ui.item,
					replaced = ui.item.prev();
				if (replaced.length == 0) {
					replaced = ui.item.next();
				}
				if ($('input[name=shuffle]').is(':checked')) {
				} else {
					$( 'input[name=shuffle]' ).prop('checked', true).change();
				}
				$('#schedule-update button[type=submit]').click();
				setTimeout(function(){
					$('input[name=sorting]').val('').change();
				}, 1000);
			}
		});
	});
	$(window).on('load', function(){
		setTimeout(function(){
			$('.list-group.post_list').css({
				'opacity': '1',
			});
		}, 100);
	})

	$('.dLabelbutton').each(function(){
		var vas = $(this);
		vas.click(function(){
			$('.media-right.media-middle').removeClass('upit');
			$(this).parents('.media-right.media-middle').addClass('upit');
		})
	});
	
	if($(window).width() < 768) {
    	$('.dropdown .dropdown-menu.dropdown-pop').css({
    	    'min-width': 'initial',
    	    'width': $(window).width() - 50 + 'px',
    	})
	}



	
	</script>
</body>
</html>
