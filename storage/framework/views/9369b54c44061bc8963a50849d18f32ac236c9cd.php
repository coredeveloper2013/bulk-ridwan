			<style type="text/css">
				.group-page .social-accounts li input + label img {
			        -webkit-filter: grayscale(100%);
			                filter: grayscale(100%);
				}	
				.group-page .social-accounts li input:checked + label img {
			        -webkit-filter: grayscale(0%);
			                filter: grayscale(0%);
				}
			</style>
			<nav class="navbar">
			  	<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->

					<ul class="nav navbar-nav">
					<li style="float: left;"> <span class="navbar-brand group-name-update"><?php echo e($group->name); ?></span> </li>
				        <li  style="float: left;" class="dropdown">
				          	<button class="dropdown-toggle btn btn-default navbar-btn btn-radius" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-pencil"></i>  </button>
					          <ul class="dropdown-menu dropdown-center dropdown-pop">
									<li>
										<form id="group-name-update" method="POST">
											<?php echo e(csrf_field()); ?>

											<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
											<div class="form-group">
												<input type="text" class="form-control" name="group_name" value="<?php echo e($group->name); ?>">
											</div>
<hr>


											<div class="form-group">
												<input type="text" class="form-control" placeholder="UTM Campaign *" name="utm_camp" value="<?php echo e(unserialize($group->utm)['utm_camp']); ?>">
											</div>

											<div class="form-group">
												<input type="text" class="form-control" placeholder="UTM Source *" name="utm_source" value="<?php echo e(unserialize($group->utm)['utm_source']); ?>">
											</div>


											<div class="form-group">
												<input type="text" class="form-control" placeholder="UTM Medium *" name="utm_med" value="<?php echo e(unserialize($group->utm)['utm_med']); ?>">
											</div>


											<div class="form-group">
												<input type="text" class="form-control" placeholder="UTM Content" name="utm_cont" value="<?php echo e(unserialize($group->utm)['utm_cont']); ?>">
											</div>

											<button class="btn btn-default btn-block" type="submit">Update</button>
										</form>
									</li>
					          	</ul>
				        </li>




						<?php if($group->type=='curation'): ?>

						<li  style="float: left;" class="dropdown">
							<button class="dropdown-toggle btn btn-default navbar-btn btn-radius" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-rss"></i>  </button>
							<ul class="dropdown-menu dropdown-center dropdown-pop dropdown-addrss">
								<li>RSS URL</li>
								<li>
									<table class="table rsslinks">
										<?php $__currentLoopData = unserialize($group->files_links)['link']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $link): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<tr><td><?php echo e($link); ?></td><td>
											<form id="delete-post-by-rsslink-<?php echo e(rand()); ?>" class="delete-post-by-rsslink pull-right" method="POST">
												<?php echo e(csrf_field()); ?>

												<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
												<input type="hidden" name="rsslink" value="<?php echo e($link); ?>">
												<button class="btn btn-default pull-right" type="submit"><i class="fa fa-trash"></i></button>
											</form></td>
										</tr>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</table>
									<form id="add-curation-online-ingroup" class="add-curation-online-ingroup" method="POST">
										<?php echo e(csrf_field()); ?>

										<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
										<div class="form-group">
											<input type="url" class="form-control" name="url" id="url" placeholder="Enter the RSS feed URL to curate content from here...">
											<button type="submit" class="btn btn-default pull-right">+</button>
										</div>
									</form>
								</li>
								<li>

								<form id="curation-refresh" class="curation-refresh" method="POST">
									<?php echo e(csrf_field()); ?>

									<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
									<button type="submit" class="btn  navbar-btn width-btn btn-center btn-dc"> Refresh Content</button>
								</form>

								</li>
							</ul>
						</li>
						<?php endif; ?>

				        <li  style="float: left;" class="dropdown">
				          	<button class="dropdown-toggle btn btn-default navbar-btn btn-radius" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i>  </button>
				            <ul class="dropdown-menu dropdown-center dropdown-pop group-list">
								
								<?php if($group->status =='0' && $group->type =='upload'): ?>
									<?php $__currentLoopData = $user->groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($g->type == 'upload'  && $g->status =='0'): ?>
											<li><a href="<?php echo e(route('content-pending', $g->id)); ?>"><?php echo e($g->name); ?></a></li>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>

								<?php if($group->status =='1' && $group->type =='upload'): ?>
									<?php $__currentLoopData = $user->groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($g->type == 'upload'  && $g->status =='1'): ?>
											<li><a href="<?php echo e(route('content-active', $g->id)); ?>"><?php echo e($g->name); ?></a></li>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
								<?php if($group->status =='2' && $group->type =='upload'): ?>
									<?php $__currentLoopData = $user->groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($g->type == 'upload'  && $g->status =='2'): ?>
											<li><a href="<?php echo e(route('content-completed', $g->id)); ?>"><?php echo e($g->name); ?></a></li>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>


								<?php if($group->status =='0' && $group->type =='curation'): ?>
									<?php $__currentLoopData = $user->groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($g->type == 'curation'  && $g->status =='0'): ?>
											<li><a href="<?php echo e(route('content-pending', $g->id)); ?>"><?php echo e($g->name); ?></a></li>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
								<?php if($group->status =='1' && $group->type =='curation'): ?>
									<?php $__currentLoopData = $user->groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($g->type == 'curation'  && $g->status =='1'): ?>
											<li><a href="<?php echo e(route('content-active', $g->id)); ?>"><?php echo e($g->name); ?></a></li>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
								<?php if($group->status =='2' && $group->type =='curation'): ?>
									<?php $__currentLoopData = $user->groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($g->type == 'curation'  && $g->status =='2'): ?>
											<li><a href="<?php echo e(route('content-completed', $g->id)); ?>"><?php echo e($g->name); ?></a></li>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>

								<?php if($group->status =='0' && $group->type =='rss-automation'): ?>
									<?php $__currentLoopData = $user->groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($g->type == 'rss-automation'  && $g->status =='0'): ?>
											<li><a href="<?php echo e(route('content-pending', $g->id)); ?>"><?php echo e($g->name); ?></a></li>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
								<?php if($group->status =='1' && $group->type =='rss-automation'): ?>
									<?php $__currentLoopData = $user->groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($g->type == 'rss-automation'  && $g->status =='1'): ?>
											<li><a href="<?php echo e(route('content-active', $g->id)); ?>"><?php echo e($g->name); ?></a></li>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>
								<?php if($group->status =='2' && $group->type =='rss-automation'): ?>
									<?php $__currentLoopData = $user->groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $g): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<?php if($g->type == 'rss-automation'  && $g->status =='2'): ?>
											<li><a href="<?php echo e(route('content-completed', $g->id)); ?>"><?php echo e($g->name); ?></a></li>
										<?php endif; ?>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<?php endif; ?>

				          	</ul>
				        </li>

						
					</ul>
					<ul class="nav navbar-nav navbar-right">



						<li class="dropdown">
				          	<button class="dropdown-toggle btn btn-default navbar-btn width-btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Recycle Posts  <i class="fa fa-angle-down"></i></button>
				            <ul class="dropdown-menu dropdown-center dropdown-pop group-list">
							<li>
								
									<form id="recycle-group-update" method="POST" class="container-fluid">
										<h4>Recycle Option</h4>
										<?php echo e(csrf_field()); ?>

										<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
										<input class="check-toog" type="checkbox" name="recycle" id="recycle" <?php if($group->recycle =='1'): ?> checked <?php endif; ?> >
										<label for="recycle">
											Recycle Posts 
										</label>
									</form>
<br>
							</li>

				            </ul>
				        </li>





						<li>
							<div class="btn btn-default navbar-btn width-btn">
								<form id="shuffle-group-update" method="POST">
									<?php echo e(csrf_field()); ?>

									<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
									<input class="check-toog" type="checkbox" name="shuffle" id="shuffle" <?php if($group->shuffle =='1'): ?> checked <?php endif; ?> >
									<label for="shuffle">
										Shuffle Posts 
									</label>
								</form>
							</div>
						</li>
<!--
						<?php if($group->type=='rss-automation'): ?>

						<li>
							<div class="btn btn-default navbar-btn width-btn">
								<form id="addimage-group-update" method="POST">
									<?php echo e(csrf_field()); ?>

									<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
									<input class="check-toog" type="checkbox" name="addimage" id="addimage">
									<label for="addimage">
										Auto Add Image
									</label>
								</form>
							</div>
						</li>
						<?php endif; ?>
-->
						<?php if($group->type!='rss-automation'): ?>

						<li class="dropdown">
							<button class="btn btn-default navbar-btn width-btn dropdown-toggle" id="Hashtags" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Hashtags <i class="fa fa-angle-down"></i></button>
							<ul class="dropdown-menu dropdown-center" aria-labelledby="Hashtags">
								<li>
									<form class="container-fluid" id="hashtags-update" method="POST" action="">
										<?php echo e(csrf_field()); ?>

										<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
										<div class="hashtags" style="width: 320px;">
											<h4>Entire Hashtags : </h4>
											<ul class="list-unstyled">
												<li>
													<div class="form-group inline-form-group">
														<span class="fa fa-facebook"></span> <input style="width: 85%;" class="form-control" type="text" name="fhash" value="<?php if(isset(unserialize($group->hash)['fb'])): ?><?php echo e(unserialize($group->hash)['fb']); ?><?php endif; ?>">  
													</div>
												</li>
												<li>
													<div class="form-group inline-form-group">
														<span class="fa fa-google-plus"></span> <input style="width: 85%;" class="form-control" type="text" name="thash" value="<?php if(isset(unserialize($group->hash)['g'])): ?><?php echo e(unserialize($group->hash)['g']); ?><?php endif; ?>">  
													</div>
												</li>
												<li>
													<div class="form-group inline-form-group">
														<span class="fa fa-linkedin"></span> <input style="width: 85%;" class="form-control" type="text" name="lhash" value="<?php if(isset(unserialize($group->hash)['in'])): ?><?php echo e(unserialize($group->hash)['in']); ?><?php endif; ?>">  
													</div>
												</li>												
												<li>
													<div class="form-group inline-form-group">
														<span class="fa fa-twitter"></span> <input style="width: 85%;" class="form-control" type="text" name="ghash" value="<?php if(isset(unserialize($group->hash)['tw'])): ?><?php echo e(unserialize($group->hash)['tw']); ?><?php endif; ?>">  
													</div>
												</li>

											</ul>
											<div class="form-group text-center">
												<button type="submit" class="btn  width-btn btn-dc">Save</button>
											</div>
										</div>
									</form>
								</li>
							</ul>
						</li>
						<?php endif; ?>
						<li class="dropdown">
							<button class="btn btn-default navbar-btn width-btn dropdown-toggle" id="Schedule" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Schedule <i class="fa fa-angle-down"></i></button>
							<?php echo $__env->make('group.schidule', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

						</li>
						<?php if($group->status == '0'): ?>
						<li>
							<div class="btn  navbar-btn width-btn btn-dc">
								<form id="change-group-status" method="POST">
									<?php echo e(csrf_field()); ?>

									<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
									<input class="check-toog" type="checkbox" name="activate" id="activate" > 
									<label for="activate">
										Activate
									</label>
								</form>
							</div>
						</li>
						<?php endif; ?>
						<?php if($group->status == '1'): ?>
						<li>
							<div>
								<form id="save-group" method="POST">
									<?php echo e(csrf_field()); ?>

									<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
									<button class="btn  navbar-btn width-btn btn-dc" type="button">
										Save
									</button>
								</form>
							</div>
						</li>
						<?php endif; ?>
						<?php if($group->status == '2'): ?>
						<li>
							<div>
								<form id="re-active-group" method="POST">
									<?php echo e(csrf_field()); ?>

									<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
									<button class="btn navbar-btn width-btn btn-dc" type="submit">
										Re-activate
									</button>
								</form>
							</div>
						</li>
						<?php endif; ?>
					</ul>
			  	</div><!-- /.container-fluid -->
				<div class="social-accounts">
					<form id="target-social-accounts" method="POST">
					<?php echo e(csrf_field()); ?>

					<input type="hidden" name="group_id" value="<?php echo e($group->id); ?>">
					<ul class="list-inline">
					 <?php $__currentLoopData = $user->socialaccounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $accounts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<li>
							
							<input type="checkbox" name="social_accounts[<?php echo e($key); ?>]" id="social-accounts-<?php echo e($accounts->id); ?>" value="<?php echo e($accounts->id); ?>"  <?php if(in_array($accounts->id, unserialize($group->target_acounts)) AND $accounts->status==1): ?> checked <?php endif; ?> <?php if($accounts->status==0): ?> disabled <?php endif; ?>> 
							<label for="social-accounts-<?php echo e($accounts->id); ?>"> 
								<span class="fa fa-<?php echo e($accounts->type); ?>"></span> 
								<img width="60" src="<?php echo e($accounts->avatar); ?>">
							</label>

						</li>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>;
					</ul>
					<button type="submit" class="hidden"></button>
					</form>
				</div>			  
			</nav>
